import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    render() {
        return (
            <EquipmentTable equipments={EQUIPMENTS}/>);
    }
}

export default App;


class EquipmentAdder extends React.Component {
    render() {
        return (
            <tr>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <td><input type="submit" value="Добавить"/></td>
            </tr>
        );
    }
}

class EquipmentRow extends React.Component {
    render() {
        return (
            <tr key={this.props.equipment.id}>
                <td>{this.props.equipment.type}</td>
                <td>{this.props.equipment.inventoryNumber}</td>
                <td>{this.props.equipment.model}</td>
                <td>{this.props.equipment.serialNumber}</td>
                <td><input type="button" value="Удалить" onClick={() => this.onDelete(this.props.equipment.id)}/></td>
            </tr>
        );
    }

    onDelete(id) {
        console.log('delete ' + id);
    }
}

class EquipmentTable extends React.Component {
    render() {
        const rows = [];
        this.props.equipments.forEach(equipment => {
            rows.push(<EquipmentRow equipment={equipment}/>);
        });
        return (
            <div>
                <form>
                    <input type="text"/>
                </form>
                <table>
                    <thead>
                    <tr>
                        <th>Тип</th>
                        <th>Инвентарный номер</th>
                        <th>Марка</th>
                        <th>Серийный номер</th>
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    <EquipmentAdder/>
                    </tbody>
                </table>
            </div>
        );
    }
}

const EQUIPMENTS = [
    {
        id: 0,
        type: "laptop",
        inventoryNumber: "33333333",
        model: "test",
        serialNumber: "213123"
    },
    {
        id: 1,
        type: "laptop",
        inventoryNumber: "33333333",
        model: "test",
        serialNumber: "213123"
    }
];